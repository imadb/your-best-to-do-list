# YourBestToDoList

Ce projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 11.0.4.

## Serveur de développement

Exécutez `ng serve` pour lancer le serveur de développement. Accédez à `http://localhost:4200/`. L'application se rechargera automatiquement si vous modifiez l'un des fichiers source.

## Builder le projet

Exécutez `ng build` pour générer le projet. Le resultat sera stocké dans le répertoire `dist/`. Utilisez l'indicateur `--prod` pour une version de production.

## les fonctionlité du projet

ce projet de ToDo List est sous le nom **Your Best ToDo List**, il contient les fonctionalité suivantes:
- ajouter vos propre tâches (avec le bouton `+` et avec le clavier `bouton Entrée`)
- lister vos tâches
- supprimer une tâche
- marquer une tâche
- filtrer vos tâches par actif/complété
- supprimer tout les tâches
- un compteur qui calcule le nombre de tâches complétées parmis tous les tâches
- la liste des tâches sera stocké automatiquement sur le localStorage, aussi pour chaque modification
- l'application elle est résponsive pour une utilisation meilleure sur plusieurs platforme.
